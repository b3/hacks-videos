# Des bricoles pour manipuler des fichiers vidéos

On trouve ici quelques scripts et modèles permettant de manipuler des
fichiers vidéos.

## Scripts

- [`decouper-videos`](decouper-videos) permet de découper un gros
  fichier en plusieurs petits et d'y accoller en tête et en fin une
  image.

## Auteurs

Copyright (C) 2021-2023 Bruno BEAUFILS

Ces bricolages ont été écrit par Bruno BEAUFILS.

Les scripts et modèles disponibles ici sont mis à dispostion sous les termes
de la licence [WTFPL](WFTPL) ou de leur propre licence quand ils ont été écrits
par d'autres auteurs.

